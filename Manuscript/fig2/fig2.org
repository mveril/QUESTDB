** Initialize R packages
   #+begin_src R :results output :session *R* :exports code                                   
library(ggplot2)                                                                              
library(latex2exp)                                                                            
library(extrafont)                                                                            
library(RColorBrewer)                                                                         
loadfonts()                                                                                   
   #+end_src 

   #+RESULTS:
   : 
   : Registering fonts with R

** Read data
   #+begin_src R :results output :session *R* :exports both
df <- read.table("data_histogram_paper");
df$x <- df$V1
df$y <- df$V2
df2 <- read.table("data_gaussian_histogram_paper");
spline.d <- as.data.frame(spline(df2$V1, df2$V2))
summary(spline.d)
   #+end_src

   #+RESULTS:
   : 
   :        x                  y            
   :  Min.   :-0.05818   Min.   :0.000e+00  
   :  1st Qu.:-0.02909   1st Qu.:2.000e-08  
   :  Median : 0.00000   Median :1.213e-04  
   :  Mean   : 0.00000   Mean   :3.093e-02  
   :  3rd Qu.: 0.02909   3rd Qu.:3.011e-02  
   :  Max.   : 0.05818   Max.   :1.873e-01

#+begin_src R :results output graphics :file (org-babel-temp-file "figure" ".png") :exports both :width 600 :height 400 :session *R* 
p <- ggplot(data=df, aes(x, y)) +
  geom_bar(stat="identity", fill="steelblue")
p <- p+ geom_line(data=spline.d, lwd=1, linetype="dashed")
p <- p + scale_x_continuous(name=TeX("$X^{(m)}$"))
p <- p + scale_y_continuous(name=TeX("Frequency"))
p <- p + theme(text = element_text(size = 20, family="Times"),                                
               legend.position = c(.20, .20),                                                 
               legend.title = element_blank())
p
   #+end_src     

   #+RESULTS:
   [[file:/tmp/babel-nBBwmV/figureJJu58N.png]]

* Export to pdf                                                                              
   #+begin_src R :results output :session *R* :exports code                                   
pdf("fig2.pdf", family="Times", width=8, height=5)                           
p                                                                                             
dev.off()                                                                                     
   #+end_src      

   #+RESULTS:
   : 
   : png 
   :   2
